package com.wangyideng.blog.controller;

import com.google.common.collect.ImmutableMap;
import com.wangyideng.blog.result.Result;
import com.wangyideng.blog.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by liushanping on 15/12/4.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Resource
    private IUserService userService;

    @RequestMapping(value = "/getSimpleList", method = RequestMethod.GET)
    public Map<String, Object> getSimpleList(@RequestParam("orderId") Integer orderId) {

//        int userId = Constants.USER_ID;
//        int addResult = userService.add(new UserModel(orderId, userId));
//        int updateResult = userService.update(orderId, userId + 100);
//        LOGGER.info(addResult + "," + updateResult);
        Result result = new Result();
        result.setData(userService.get(orderId));
        // 返回结果
        return ImmutableMap.<String, Object>builder().put("data", result).build();

    }

//
//    @RequestMapping(value = "/getSimpleDetail/{id}", method = RequestMethod.GET)
//    public Map<String, Object> toUpdate(@PathVariable("id") Integer id) {
//        Result result = new Result();
//        result.setData(id);
//        return ImmutableMap.<String, Object>builder().put("data", result).build();
//    }
}
