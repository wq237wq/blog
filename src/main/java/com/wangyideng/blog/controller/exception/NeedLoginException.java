package com.wangyideng.blog.controller.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by liushanping on 15/12/11.
 */
public class NeedLoginException extends APIException {
    private static final long serialVersionUID = 719058741716618315L;

    public NeedLoginException() {
        super("请先登录。", HttpStatus.FORBIDDEN.value());
    }
}
