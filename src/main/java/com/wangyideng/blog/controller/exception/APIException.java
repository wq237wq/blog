package com.wangyideng.blog.controller.exception;

import com.google.common.collect.Lists;
import org.apache.commons.collections.ListUtils;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * Created by liushanping on 15/12/10.
 */
public class APIException extends RuntimeException {
    private static final long serialVersionUID = 890566201163111011L;
    @SuppressWarnings("unchecked")
    private List<Object> params = ListUtils.EMPTY_LIST;
    private String msgTemplate;
    protected int statusCode = HttpStatus.BAD_REQUEST.value();

    public APIException(String msgTemplate, int statusCode, Object... params) {
        this.msgTemplate = msgTemplate;
        this.statusCode = statusCode;
        this.params = Lists.newArrayList(params);
    }

    public String getFormattedMessage() {
        return String.format(msgTemplate, params.toArray());
    }

    public int getStatusCode() {
        return statusCode + 10000;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

}

