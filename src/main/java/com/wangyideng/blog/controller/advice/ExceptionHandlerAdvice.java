package com.wangyideng.blog.controller.advice;

import com.google.common.collect.ImmutableMap;
import com.wangyideng.blog.controller.exception.APIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by liushanping on 15/12/10.
 */
@RestController
@ControllerAdvice("com.wangyideng.blog.controller")
public class ExceptionHandlerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);


    @ExceptionHandler(value = APIException.class)
    public Map<String, Object> apiExceptionHandler(APIException ex, HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        String message = ex.getFormattedMessage();
        int code = ex.getStatusCode();
        Map<String, Object> error = ImmutableMap.<String, Object>builder().put("request", url).put("code", code).put("message", message).build();
        return ImmutableMap.<String, Object>builder().put("error", error).build();
    }

    @ExceptionHandler
    public Map<String, Object> defaultExceptionHandler(Throwable ex, HttpServletRequest request) {
        LOGGER.error(ex.getMessage(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "服务器内部错误";
        Map<String, Object> error = ImmutableMap.<String, Object>builder().put("request", request.getRequestURI())
                .put("code", HttpStatus.INTERNAL_SERVER_ERROR.value()).put("message", message).build();
        return ImmutableMap.<String, Object>builder().put("error", error).build();
    }
}
