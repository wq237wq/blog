package com.wangyideng.blog.controller;

import com.google.common.collect.ImmutableMap;
import com.wangyideng.blog.result.Result;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * Created by liushanping on 15/12/4.
 */
@RestController
@RequestMapping("/chords")
public class ChordsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChordsController.class);


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Map<String, Object> getChord() {

        Result result = new Result();
        // 返回结果
        return ImmutableMap.<String, Object>builder().put("data", result).build();

    }
}
