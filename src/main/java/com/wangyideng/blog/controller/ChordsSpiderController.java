package com.wangyideng.blog.controller;

import com.google.common.collect.ImmutableMap;
import com.wangyideng.blog.constants.Constants;
import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.result.Result;
import com.wangyideng.blog.service.IChordService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Map;

/**
 * Created by liushanping on 15/12/4.
 */
@RestController
@RequestMapping("/spider")
public class ChordsSpiderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChordsSpiderController.class);

    @Resource
    private IChordService chordService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Map<String, Object> getChord() {

        String chordOriginalId = "24156";
        String threadUrl = Constants.JITASHE_NET_DOMAIN + "/tab/" + chordOriginalId + "/";
        Document doc = null;
        try {
            doc = Jsoup.connect(threadUrl).get();
            Elements images = doc.getElementsByClass("alphaTab");
            Elements imgList = images.get(0).getElementsByTag("img");

            String imageOriginUrl = "";

            for (Element imgElement : imgList) {
                imageOriginUrl = imgElement.attr("src");
                String nameAndArtist = imgElement.attr("alt");
                String[] nameAndArtistArr = nameAndArtist.split("_");
                String chordName = nameAndArtistArr[0];
                String artist = nameAndArtistArr[1];
                System.out.println(imageOriginUrl);
                String imageSavedUrl = downloadFile(Constants.JITASHE_NET_DOMAIN + imageOriginUrl, chordOriginalId);

                ChordDo cd = new ChordDo();
                cd.setImageUrl(imageSavedUrl);
                cd.setUserId(1);
                cd.setArticleId(1);
                cd.setOriginalUrl(Constants.JITASHE_NET_DOMAIN + imageOriginUrl);
                cd.setName(chordName);
                chordService.insert(cd);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        Result result = new Result();
        // 返回结果
        return ImmutableMap.<String, Object>builder().put("data", result).build();

    }

    private String downloadFile(String imagePath, String fileName) {
        String savedPath = "";
        try {
            URL website = new URL(imagePath);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = null;

             savedPath = Constants.GUITAR_IMAGE_FOLDER + fileName + "." + getFilePostFix(imagePath);
            fos = new FileOutputStream(savedPath);

            System.out.println("file saved to: " + savedPath);

            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedPath;
    }

    private File getFileName(String imagePath) {
        String[] urlArr = imagePath.split("/");
        String fileName = urlArr[urlArr.length - 1];
        System.out.println("file saved to: " + Constants.GUITAR_IMAGE_FOLDER + fileName);
        return new File(Constants.GUITAR_IMAGE_FOLDER + fileName);
    }

    private String getFilePostFix(String imagePath) {
        String[] urlArr = imagePath.split("\\.");
        String postFix = urlArr[urlArr.length - 1];
        return postFix;
    }
}
