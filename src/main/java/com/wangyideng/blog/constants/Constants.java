package com.wangyideng.blog.constants;

/**
 * Created by liushanping on 15/12/4.
 */
public class Constants {
    //user id
    public static final int USER_ID = 23;

    public static final String JITASHE_NET_DOMAIN = "http://www.jitashe.net";

    public static final String GUITAR_IMAGE_FOLDER = "/Users/qing/Documents/dev/guitar_image_store/";
}
