package com.wangyideng.blog.result;

import java.io.Serializable;

/**
 * @Description
 * @Author liushanping
 * @Created 15/12/9
 * @Version 1.0
 */
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1385455802067879550L;

    boolean success;
    int errCode;
    String errMsg;
    T data;

    public Result(boolean success, int errCode, String errMsg) {
        this.setSuccess(success);
        this.setErrCode(errCode);
        this.setErrMsg(errMsg);
    }

    public Result(boolean success, T data) {
        this.setSuccess(success);
        this.setData(data);
    }

    public Result(boolean success) {
        this.setSuccess(success);
    }

    public Result() {
        success = true;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result{" +
                "data=" + data +
                ", success=" + success +
                ", errCode=" + errCode +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}
