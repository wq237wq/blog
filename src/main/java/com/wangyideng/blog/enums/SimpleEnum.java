package com.wangyideng.blog.enums;

/**
 * Created by liushanping on 15/12/4.
 */
public enum  SimpleEnum {
    EXPIRED(1, "超时未支付"),
    CANCELLED(2, "订单已取消"),
    DELETED(3, "订单被删除");

    private int value;
    private String desc;

    SimpleEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
