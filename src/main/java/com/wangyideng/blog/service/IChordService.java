package com.wangyideng.blog.service;

import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.model.ChordModel;

/**
 * Created by wq on 15/12/4.
 */
public interface IChordService {

    ChordModel get(Integer id);

    int insert(ChordDo chordDo);
}
