package com.wangyideng.blog.service;

import com.wangyideng.blog.model.UserModel;

/**
 * Created by wq on 15/12/4.
 */
public interface IUserService {
    UserModel get(Integer orderId);
}
