package com.wangyideng.blog.service.impl;

import com.wangyideng.blog.dao.ChordDAOAdapter;
import com.wangyideng.blog.dao.UserDAOAdapter;
import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.model.ChordModel;
import com.wangyideng.blog.model.UserModel;
import com.wangyideng.blog.service.IChordService;
import com.wangyideng.blog.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by liushanping on 15/12/4.
 */
@Service("chordService")
public class ChordServiceImpl implements IChordService {

    @Resource
    private ChordDAOAdapter chordDAOAdapter;


    @Override
    public ChordModel get(Integer orderId) {

        return chordDAOAdapter.get(orderId);

    }

    @Override
    public int insert(ChordDo chordDo) {
        return chordDAOAdapter.insert(chordDo);
    }


}
