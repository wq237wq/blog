package com.wangyideng.blog.service.impl;

import com.wangyideng.blog.dao.UserDAOAdapter;
import com.wangyideng.blog.model.UserModel;
import com.wangyideng.blog.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by liushanping on 15/12/4.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Resource
    private UserDAOAdapter userDAOAdapter;


    @Override
    public UserModel get(Integer orderId) {

        return userDAOAdapter.get(orderId);

    }
}
