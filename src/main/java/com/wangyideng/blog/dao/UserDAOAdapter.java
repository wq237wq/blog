package com.wangyideng.blog.dao;

import com.wangyideng.blog.dao.db.UserDoMapper;
import com.wangyideng.blog.domain.UserDo;
import com.wangyideng.blog.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by liushanping on 15/12/1.
 */
@Service("userDAOAdapter")
public class UserDAOAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAOAdapter.class);

    @Resource
    private UserDoMapper userDAO;

//    public int add(UserModel userModel) {
//        LOGGER.info("test add");
//        UserDo userDo = new UserDo();
//        return userDAO.add(userDo);
//    }
//
//    public int update(long orderId, int userId) {
//        LOGGER.info("test update");
//        return userDAO.update(orderId, userId);
//    }

    public UserModel get(Integer id) {
        LOGGER.info("test get");
        UserDo userDo = userDAO.selectByPrimaryKey(id);
        return new UserModel(userDo);
    }
}
