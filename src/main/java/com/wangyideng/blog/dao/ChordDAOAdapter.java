package com.wangyideng.blog.dao;

import com.wangyideng.blog.dao.db.ChordDoMapper;
import com.wangyideng.blog.dao.db.UserDoMapper;
import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.domain.UserDo;
import com.wangyideng.blog.model.ChordModel;
import com.wangyideng.blog.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by wq on 15/12/1.
 */
@Service("chordDAOAdapter")
public class ChordDAOAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChordDAOAdapter.class);

    @Resource
    private ChordDoMapper chordDoMapper;

//    public int add(UserModel userModel) {
//        LOGGER.info("test add");
//        UserDo userDo = new UserDo();
//        return userDAO.add(userDo);
//    }
//
//    public int update(long orderId, int userId) {
//        LOGGER.info("test update");
//        return userDAO.update(orderId, userId);
//    }

    public ChordModel get(Integer id) {
        LOGGER.info("test get");
        ChordDo userDo = chordDoMapper.selectByPrimaryKey(id);
        return new ChordModel(userDo);
    }

    public int insert(ChordDo chordDo){
        chordDo.setArticleId(1);
        int result = chordDoMapper.insert(chordDo);
        System.out.print(result);
        return  result;
    }
}
