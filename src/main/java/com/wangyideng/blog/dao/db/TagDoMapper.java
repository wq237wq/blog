package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.TagDo;
import org.springframework.stereotype.Repository;

@Repository
public interface TagDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TagDo record);

    int insertSelective(TagDo record);

    TagDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TagDo record);

    int updateByPrimaryKey(TagDo record);
}