package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.CommentDo;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(CommentDo record);

    int insertSelective(CommentDo record);

    CommentDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CommentDo record);

    int updateByPrimaryKeyWithBLOBs(CommentDo record);

    int updateByPrimaryKey(CommentDo record);
}