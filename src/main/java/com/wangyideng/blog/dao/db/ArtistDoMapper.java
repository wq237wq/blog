package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.ArtistDo;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtistDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ArtistDo record);

    int insertSelective(ArtistDo record);

    ArtistDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ArtistDo record);

    int updateByPrimaryKey(ArtistDo record);
}