package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.DistrictDo;
import org.springframework.stereotype.Repository;

@Repository
public interface DistrictDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DistrictDo record);

    int insertSelective(DistrictDo record);

    DistrictDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DistrictDo record);

    int updateByPrimaryKey(DistrictDo record);
}