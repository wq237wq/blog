package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.UserDo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserDo record);

    int insertSelective(UserDo record);

    UserDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserDo record);

    int updateByPrimaryKey(UserDo record);
}