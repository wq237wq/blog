package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.ArticleDo;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ArticleDo record);

    int insertSelective(ArticleDo record);

    ArticleDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ArticleDo record);

    int updateByPrimaryKeyWithBLOBs(ArticleDo record);

    int updateByPrimaryKey(ArticleDo record);
}