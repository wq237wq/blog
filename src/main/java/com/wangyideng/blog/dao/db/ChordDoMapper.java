package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.ChordDo;
import org.springframework.stereotype.Repository;

@Repository
public interface ChordDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChordDo record);

    int insertSelective(ChordDo record);

    ChordDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChordDo record);

    int updateByPrimaryKey(ChordDo record);
}