package com.wangyideng.blog.dao;

import com.wangyideng.blog.dao.db.ArticleDoMapper;
import com.wangyideng.blog.dao.db.ChordDoMapper;
import com.wangyideng.blog.domain.ArticleDo;
import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.model.ArtistModel;
import com.wangyideng.blog.model.ChordModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by wq on 15/12/1.
 */
@Service("artictDAOAdapter")
public class ArtistDAOAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistDAOAdapter.class);

    @Resource
    private ArticleDoMapper articleDoMapper;

//    public int add(UserModel userModel) {
//        LOGGER.info("test add");
//        UserDo userDo = new UserDo();
//        return userDAO.add(userDo);
//    }
//
//    public int update(long orderId, int userId) {
//        LOGGER.info("test update");
//        return userDAO.update(orderId, userId);
//    }

    public int insert(ArticleDo articleDo){
        return articleDoMapper.insert(articleDo);
    }

    public ArtistModel get(Integer id) {
        LOGGER.info("test get");
        ArticleDo userDo = articleDoMapper.selectByPrimaryKey(id);
        return new ArtistModel(userDo);
    }
}
