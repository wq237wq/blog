package com.wangyideng.blog.job;

import com.wangyideng.blog.service.IUserService;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by liushanping on 15/12/4.
 */
@Service
public class SimpleJob {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SimpleJob.class);

    @Resource
    private IUserService simpleService;

//    @Scheduled(cron = "0/25 * * * * ?")
//    public void simpleTask() {
//        LOGGER.info("simpleTask start");
//        System.out.println("simpleTask start");
//        simpleService.add(new UserModel(223, 43245));
//        LOGGER.info("simpleTask end");
//    }
}
