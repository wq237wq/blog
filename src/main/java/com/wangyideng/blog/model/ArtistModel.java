package com.wangyideng.blog.model;

import com.wangyideng.blog.domain.ArticleDo;
import com.wangyideng.blog.domain.ChordDo;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wq on 15/12/4.
 */
public class ArtistModel implements Serializable {
    private static final long serialVersionUID = 1531262770107928318L;

    public ArtistModel(ArticleDo articleDo) {
        BeanUtils.copyProperties(articleDo, this);
    }
    private Integer id;

    private String name;

    private String gender;

    private Integer tagId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

}
