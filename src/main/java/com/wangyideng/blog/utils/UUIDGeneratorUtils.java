package com.wangyideng.blog.utils;

import java.util.UUID;

/**
 * Created by liushanping on 15/12/4.
 */
public class UUIDGeneratorUtils {
    public static String getUUID(){
        String s = UUID.randomUUID().toString();
        //去掉“-”符号
        return s.substring(0,8)+s.substring(9,13)+s.substring(14,18)+s.substring(19,23)+s.substring(24);
    }
}
