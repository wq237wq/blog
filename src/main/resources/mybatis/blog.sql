CREATE TABLE article
(
    id INT(11) PRIMARY KEY NOT NULL,
    title VARCHAR(45),
    content TEXT,
    created_time TIMESTAMP,
    updated_time TIMESTAMP,
    user_id INT(11) NOT NULL
);
CREATE TABLE chords
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(45),
    image_url VARCHAR(255),
    tags VARCHAR(45),
    updated_time TIMESTAMP,
    created_time TIMESTAMP,
    article_id INT(11) NOT NULL,
    user_id INT(11) NOT NULL,
    `order` INT(11)
);
CREATE TABLE comment
(
    id INT(11) PRIMARY KEY NOT NULL,
    content TEXT,
    user_id INT(11),
    created_time TIMESTAMP,
    updated_time TIMESTAMP,
    article_id INT(11) NOT NULL,
    article_userId INT(11) NOT NULL,
    reply_to_comment_id INT(11)
);
CREATE TABLE tag
(
    id INT(11) PRIMARY KEY NOT NULL,
    name VARCHAR(45),
    article_id INT(11) NOT NULL,
    created_time TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP'
);
CREATE TABLE user
(
    id INT(11) PRIMARY KEY NOT NULL,
    username VARCHAR(45),
    password VARCHAR(45),
    email VARCHAR(45),
    mobile INT(11),
    type VARCHAR(45),
    nick VARCHAR(45),
    created_time TIMESTAMP,
    updated_time TIMESTAMP,
    avatar VARCHAR(255)
);