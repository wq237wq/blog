package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.ChordDo;
import com.wangyideng.blog.domain.UserDo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by wangqing on 2015/12/21.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml", "classpath:/applicationContext-db.xml"})
public class IChordDAOTest {

    @Resource
    private ChordDoMapper chordDoMapper;

    @Test
    public void testCreate() {
    }

    @Test
    public void testGet() {
        ChordDo result = chordDoMapper.selectByPrimaryKey(1);
        System.out.println(result.getId());
    }

    @Test
    public void testInsert() {
        ChordDo cd = new ChordDo();
        cd.setArticleId(1);
        cd.setCreatedTime(new Date());
        cd.setImageUrl("test");
        cd.setUserId(1);
        int result = chordDoMapper.insert(cd);
        System.out.println(result);
    }
}