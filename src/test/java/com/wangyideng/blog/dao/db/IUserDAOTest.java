package com.wangyideng.blog.dao.db;

import com.wangyideng.blog.domain.UserDo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by wangqing on 2015/12/21.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml", "classpath:/applicationContext-db.xml"})
public class IUserDAOTest {

    @Resource
    private UserDoMapper iUserDAO;

    @Test
    public void testCreate() {
    }

    @Test
    public void testGet() {
        UserDo result = iUserDAO.selectByPrimaryKey(1);
        System.out.println(result.getId());
    }
}